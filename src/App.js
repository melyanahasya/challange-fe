import './App.css';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import Login from './Page/Login';
import Home from './Page/Home';
import Register from './Page/Register';

function App() {
  return (
    <div>
    <div>
      
      <BrowserRouter>
        <main>
          <Switch>
            <Route path="/" component={Login} exact />
            <Route path="/register" component={Register} exact />
            <Route path="/home" component={Home} exact />
          </Switch>
        </main>
      </BrowserRouter>
    </div>
  </div>
  );
}

export default App;
