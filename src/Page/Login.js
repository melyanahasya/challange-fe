import axios from "axios";
import React, { useState } from "react";
import { useHistory } from "react-router-dom";
import "../style/login.css";
import Swal from "sweetalert2";

export default function () {
  const [username, setUsername] = useState("");
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const history = useHistory();


const login = async (e) => {
    e.preventDefault();

    try {
     const {status} = await axios.post("http://localhost:8000/sign-in", {
        username,
        email,
        password,
      });
      
     if (status === 200) {
        Swal.fire({
          icon: "success",
          title: "Login Berhasil ",
          showConfirmButton: false,
          timer: 1500,
        });
        history.push("/home");
        setTimeout(()=>{
          window.location.reload();
          }, 1500)
      }
    } catch (err) {
      console.log(err);
    }
  };
  return (
    <div className="registration-form">
      <form onSubmit={login}>
        <div className="form-group">
          <input
            type="text"
            className="form-control item"
            id="username"
            placeholder="Username"
            value={username}
            required
            onChange={(e) => setUsername(e.target.value)}
          />
        </div>
        <div className="form-group">
          <input
            type="text"
            className="form-control item"
            id="email"
            placeholder="Email"
            value={email}
            required
            onChange={(e) => setEmail(e.target.value)}
          />
        </div>
        <div className="form-group">
          <input
            type="password"
            className="form-control item"
            id="password"
            placeholder="Password"
            value={password}
            required
            onChange={(e) => setPassword(e.target.value)}
          />
        </div>
     
        <div>
          <div className="form-group">
            <button type="submit" className="btn btn-block create-account">
              Login
            </button>
          </div>

          <div>
            <p>
              Belum punya akun? silahkan <a href="/register">Register</a>
            </p>
          </div>
        </div>
      </form>
    </div>
  );
}
