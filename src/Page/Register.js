import axios from "axios";
import React, { useState } from "react";
import { useHistory } from "react-router-dom";
import "../style/register.css";
import Swal from "sweetalert2";

export default function Register() {
  const [username, setUsername] = useState("");
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const history = useHistory();

//   const register = async (e) => {
//     e.preventDefault();
//     e.persist();

//     try {
//       await axios.post("http://localhost:8000/sign-up", {
//         username: username,
//         email: email,
//         password: password,
//       });
//     } catch (error) {
//       Swal.fire({
//         icon: "error",
//         title: "Username atau password tidak valid",
//         showCancelButton: false,
//         timer: 1500,
//       });
//       history.push("/");
//       window.location.reload();

//       console.log(error);
//     }
//   };

const register = async (e) => {
    e.preventDefault();
    e.persist();

    try {
      await axios.post("http://localhost:8000/sign-up", {
        username,
        email,
        password,
      });
      Swal.fire({
        icon: "success",
        title: "Berhasil Register ",
        showConfirmButton: false,
        timer: 1500,
      });
      setTimeout(() => {
        history.push("/");
        window.location.reload();
      }, 1250);
    } catch (err) {
      console.log(err);
    }
  };
  return (
    <div className="registration-form">
      <form onSubmit={register}>
        <div className="form-group">
          <input
            type="text"
            className="form-control item"
            id="username"
            placeholder="Username"
            value={username}
            required
            onChange={(e) => setUsername(e.target.value)}
          />
        </div>
        <div className="form-group">
          <input
            type="text"
            className="form-control item"
            id="email"
            placeholder="Email"
            value={email}
            required
            onChange={(e) => setEmail(e.target.value)}
          />
        </div>
        <div className="form-group">
          <input
            type="password"
            className="form-control item"
            id="password"
            placeholder="Password"
            value={password}
            required
            onChange={(e) => setPassword(e.target.value)}
          />
        </div>

        <div className="form-group">
          <button type="submit" className="btn btn-block create-account">
            Register
          </button>
        </div>
      </form>
    </div>
  );
}
